AUTHOR = 'Richard Azia'
SITENAME = 'Main-Vision.com'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('My Blog', 'https://www.main-vision.com/richard/blog/'),
         ('Home', 'https://www.main-vision.com/')
         )

# Social widget
SOCIAL = (('Me, on CalcKey.social', 'https://calckey.social/@richardazia'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 3

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True