Title: Geography Of The Roman Empire
Name: Geography Of The Roman Empire
Date: 1998-05-22 18:00
Modified: 2023-05-24 15:31
Tags: Ancient Rome, history
Category: Ancient Rome
Slug: Geography Of The Roman Empire
Author: Richard
Summary: Rome spanned from England to Europe, to surround the Mediterranean and more. 

# The Roman Empire
The Roman Empire covered much of England, Europe, surrounded the Mediterranean and more. As a result it ecompassed different altitudes, climates biomes and more. 

Following a recent e-mail I thought that it would be interesting to talk about the geography of the Roman empire, talking firstly about Italy and then about the rest of Europe, I may add some maps and images to illustrate all this.

[Rome](https://www.main-vision.com/richard/rome.php) Rome was the center of the Roman empire, it was built near the river Tiber. The city was built on seven hills. 

[Pompeii](https://www.main-vision.com/richard/pompeii.php) The City that was covered by a volcanic eruption in 79AD. 

[Nyon](https://www.main-vision.com/richard/nyon.php) A provincial town at the edge of the Léman. Léman is lake in Latin. 

[Damascus](https://www.main-vision.com/richard/damas.php) Damascus.

[Jerash](https://www.main-vision.com/richard/jerash.php) Jerash is a Roman city in Jordan. 


