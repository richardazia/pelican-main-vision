Title: Conquered Territories
Name: Conquered Territories
Date: 1998-05-22 18:00
Modified: 2023-05-24 15:50
Tags: Ancient Rome, history
Category: Ancient Rome
Slug: Conquered Territories
Author: Richard Azia
Summary: Territories Conquered By The Romans

# Conquered Territories
## Fortifying the new cities
The Roman army conquered the land and while they did that they built forts. The forts were normally wooden. They were spread around the frontier. They also had walls around thecities. The wall was built of stone and rocks. This made it very solid as it was made in a special way. The walls were made to be wide enough for soldiers to walk along them. They also opened the doors to the city. They had to pull the gates of the city up to let the people get in. The cities had different types of buildings in them. They put roads on top of it so that the vehicles from the troops can move more easily.
## Farming the land
The Romans conquered a lot of land and they had to get food to feed all the people in it so they had to find a way to feed everyone. Everyone was offered corn and if they didn't' t get it they got annoyed as it was considered a right. Caesar introduced the salary which was salt given to the people in order to thank them. The farmers were poor and lived in houses made of mud and straw or similar material. They also had some land to farm. They kept animals such as oxen or beef from which they got food and an income. Religion was an important part in the life of a farmer as he wanted a better life later. They sacrificed sheep and other animals and offered food to their gods. They looked upon the gods as those who were allowing them to make the crops grow.


[Nyon](https://www.main-vision.com/richard/nyon.php) A provincial town at the edge of the Léman. Léman is lake in Latin. 

[Damascus](https://www.main-vision.com/richard/damas.php) Damascus.

[Jerash](https://www.main-vision.com/richard/jerash.php) Jerash is a Roman city in Jordan. 

[Calpe](https://www.main-vision.com/richard/calpe.php) Jerash is a Roman city in Jordan. 