Title: Ancient Worlds, an online community about Ancient civilizations
Name: Ancient Worlds, an online community about Ancient civilizations
Date: 1998-05-22 18:00
Modified: 2023-05-24 16:35
Tags: information technology, article
Category: Information Technology
Slug: Ancient Worlds, an online community about Ancient civilizations
Author: Richard
Summary: Short version for index and feeds

For all those whom have been interested in a topic it has been a challenge to find a community which they can become part of in order to share their enjoyment. On the World Wide Web communities may be a variety of occurences. The webmastering community for example encompasses all webmasters although they may not know of each other's existence. Other communities help to join professional people in their struggle to find work. Another form of community and the one I am going to speak to you about is of that at [Ancient Worlds](http://www.ancientworlds.net) where people may congregate. 
At this location people may be guests or community members and the site is divided into various time periods. As the user becomes a member of a community, for example that of the Roman civilization then he choses a Roman name according to a group of around fourty families. Once this is completed he may become a more dynamic member of the community through sending "gram" to other members of the site. 
With a communications panel the user sees who is online and what role they have within this site, whether a simple citizen or a demigod. As people progress through the site they find discussion groups about history, some role playing and more. Within this collection of pages it is a dynamic way of learning some latin, learning about Julius Caesar and far more subjects. Having just relaunched the site is expanding at a very impressive rate to include new members all the time. 
The importance of this community is that it resembles what online communities used to be, a group of people whom collect themselves under one domain and share their interests much as geocities used to do. Every single person may format their "Domus" as they please effectively creating their own little home on the World Wide Web. 